import logo from './logo.svg';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { HeaderStyled } from './StyledComponents/Styles.style'

import Navibar from './Navibar';
import Home from './Pages/Home';
import CurrencyConverter from './Pages/CurrencyConverter';
import KompotGame from './Pages/KompotGame'
import AboutUs from './Pages/AboutUs';
import Contacts from './Pages/Contacts';
import Error404 from './Pages/Error404'


function App() {
  return (
    <div>
        <Router>
          <HeaderStyled>
            <Navibar />
          </HeaderStyled>
            <Switch>
              <Route exact path = '/' component = {Home} />
              <Route exact path = '/converter' component = {CurrencyConverter} />
              <Route exact path = '/kompot' component = {KompotGame} />
              <Route exact path = '/contacts' component = {Contacts} />
              <Route exact path = '/about' component = {AboutUs} />
              <Route component = {Error404} />
            </Switch>
        </Router>
    </div>
  );
}

export default App;
