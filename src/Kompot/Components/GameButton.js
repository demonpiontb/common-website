import React from 'react'
import { GameButtonStyled } from '../Styles/Styles.style'

export default function GameButton(props) {
    return (
        <GameButtonStyled onClick = {props.onClick}>
            {props.text}
        </GameButtonStyled>
    )
}
