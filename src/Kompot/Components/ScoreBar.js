import React from 'react'
import { ScoreBarDivStyle, ScoreDisplayBarStyle } from '../Styles/Styles.style'

export default function ScoreBar(props) {
    return (
        <ScoreBarDivStyle>
        <div>
            <ScoreDisplayBarStyle><h2>Score: {props.score}</h2></ScoreDisplayBarStyle>      
        </div>
        </ScoreBarDivStyle>
    )
}
