import React from 'react'
import { TimerBarDivStyle, TimerDisplayBarStyle } from '../Styles/Styles.style'

export default function TimerBar(props) {
    return (
        <TimerBarDivStyle>
        <div>
            <TimerDisplayBarStyle><h2>Time: {props.timer}</h2></TimerDisplayBarStyle>      
        </div>
        </TimerBarDivStyle>
    )
}
