import { React, useState } from 'react'
import '../styles.css';
import idleAnim from '../Assets/idle.gif'
import attackAnim from '../Assets/attack.gif'

export default function Player(props) {
    const [isAttacking, setIsAttacking] = useState(false)
    const [isOnCooldown, setIsOnCooldown] = useState(false)

    var attackTime = 500
    var cooldownTime = 1000
    
    //Function that is called from document.onmouseclick, changes states
    let attack = () => {
        if (!isOnCooldown){
            sendDataToGM()
            setIsAttacking(true)
            setIsOnCooldown(true)
            setTimeout(disableAttack, attackTime)
            setTimeout(disableCoolDown, cooldownTime)
        } else {
            console.log("On cooldown!")
        }

    }

    //Gets coordinates of the player and sends it to Game Manager, called on any attack
    let sendDataToGM = () => {
        let playerCoordRect = document.getElementsByClassName('player')[0].getBoundingClientRect()
        let dataToPass = {
            myId : 'PLAYER',
            myCoords : playerCoordRect
        }
        props.onClick(dataToPass)
    }

    let disableCoolDown = () => {
        setIsOnCooldown(false)
    }

    let disableAttack = () => {
        setIsAttacking(false)
    }
    
    /* Probably might be optimized with onmousemove only above game window, coming in v 1.0.2 :) */
    document.onmousemove = (e) => {
        if (props.isGameActive && document.getElementById('gameWindow')){
            var isCrossingBorder = false
            var width = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;
            //Stating boundaries of the gamefield
            var leftBoundary = ((width - 800) / 2)
            var rightBoundary = ((width - 800) / 2) + 800
            var topBoundary = 198
            var bottomBoundary = 798
            var playerHeight = 80
            var playerWidth = 40
            var x = e.clientX;
            var y = e.clientY;
            //Getting mouse position
            var newposX = x - ((width - 800) / 2) - playerWidth / 2;
            var newposY = y - 198 - playerHeight / 2;

            isCrossingBorder = (x < (leftBoundary + 18) || (x > (rightBoundary - 18)) || (y < (topBoundary + 40)) || (y > (bottomBoundary - 40))) ? true : false

            var moveToX = x < (leftBoundary + 18) ? 0 : x > (rightBoundary - 18) ? 800 - playerWidth : newposX;

            var moveToY = y < (topBoundary + playerHeight / 2) ? 0 : y > (bottomBoundary - playerHeight / 2) ? 600 - playerHeight : newposY;

            if (isCrossingBorder) { //Changing transform speed whenever border is crossed
                document.getElementsByClassName('player')[0].style.transition = "transform (.02,1.23,.79,1.08)";
            } else {
                document.getElementsByClassName('player')[0].style.transition = "transform 1s cubic-bezier(.02,1.23,.79,1.08)";
            }
            document.getElementsByClassName('player')[0].style.transform = "translate3d("+moveToX+"px,"+moveToY+"px,0px)"
        }
    }

    document.onmousedown = () => {
        if (props.isGameActive && document.getElementById('gameWindow')){
            attack()
        }
    }

    if (props.isGameActive) {
        return (
            <div>
                <div className = 'player'>
                    <img className = 'notDraggableImage' src = {isAttacking ? attackAnim : idleAnim} alt = "player" height = '80px' width = '40px'></img>
                </div>
            </div>
        )
    } else {
        return (
            <div/>
        )
    }

}
