import { Component } from 'react'
import '../styles.css';
import enemyAnim from '../Assets/walk.gif'

export default class Enemy extends Component {
    constructor(props){
        super(props)
        this.onSpawn = this.onSpawn.bind(this)
        this.sendDataToGM = this.sendDataToGM.bind(this)
        this.getRandomIntInclusive = this.getRandomIntInclusive.bind(this)
        this.state = {
            spawnDelayState : false,
            leftBoundary : 0,
            rightBoundary : 800,
            topBoundary : 0,
            bottomBoundary : 600,
            enemyHeight : 80,
            enemyWidth : 40,
            id : props.id,
            spawnStyle : {},
            walkStyle : {},
            currentX: 0,
            currentY: 0
        }
    }

    getRandomIntInclusive(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min; //Максимум и минимум включаются
      }

    onSpawn(){
        let pointY = this.getRandomIntInclusive(this.state.topBoundary, this.state.bottomBoundary - this.state.enemyHeight / 1.1)
        this.setState({
            spawnStyle : {
                transform: "translate(0px, "+ pointY +"px)",
                transition: "1ms cubic-bezier(.27,.65,.65,1)"
            },
            walkStyle : {
                transform: "translate(760px, "+ pointY +"px)",
                transition: 12 - this.props.difficulty + "s cubic-bezier(.27,.65,.65,1)"
            }
        })
        setTimeout(() => {
            this.setState({
                spawnDelayState : true
            })
        }, 500)

        setTimeout(() => {
            this.props.passedReciever(this.props.id)
        }, (12 - this.props.difficulty) * 1000)
    }

    sendDataToGM() {
        let enemyClickedRect = document.getElementById(this.state.id).getBoundingClientRect();
        let dataToPass = {
            myId : this.state.id,
            myCoords : enemyClickedRect
        }
        this.props.onClick(dataToPass)  
    }
 
    componentWillMount() {
        this.onSpawn()
    }


    render(){
        return (
            <div>
                <div className = 'enemy' style = {this.state.spawnDelayState ? this.state.walkStyle : this.state.spawnStyle} id = {this.state.id} onClick = {this.sendDataToGM}>
                    <img className = 'notDraggableImage' src = {enemyAnim} alt = "enemy" height = '80px' width = '40px'></img>
                </div>
            </div>
        )
    }

}
