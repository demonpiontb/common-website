import React, { Component } from 'react'
import Player from './Player';
import Enemy from './Enemy';
import ScoreBar from './ScoreBar'
import TimerBar from './TimerBar'
import GameButton from './GameButton';

//Main component to compute game logics and control diffuculty, enemy spawn, spawn rate, etc.
export default class Game extends Component {
    constructor(props){
        super(props)
        this.gameLoop = this.gameLoop.bind(this)
        this.collisionCheck = this.collisionCheck.bind(this)
        this.recieveAndWriteData = this.recieveAndWriteData.bind(this)
        this.getRandomIntInclusive = this.getRandomIntInclusive.bind(this)
        this.enemyPassed = this.enemyPassed.bind(this)
        this.checkScore = this.checkScore.bind(this)
        this.startButtonPressed = this.startButtonPressed.bind(this)
        this.endGame = this.endGame.bind(this)
        this.state = {
            isGameActive : false,
            isGameFinished : false,
            difficulty : 0,
            gameTimer : 0,
            enemyArray : [],
            lastEnemyAttackedId : -1,
            lastEnemyAttackedCoords : undefined,
            playerCoords : undefined,
            gameScore : 10
        }
    }

    //Function to get random ints - used to get random ID's for enemies *UNSAFE*
    getRandomIntInclusive(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
      }

    //Main function - launches intervals for spawning, changing difficulty and stops game in score is less than 0
    gameLoop() {
        this.setState({
            difficulty : 0,
            gameTimer : 0,
            enemyArray : [],
            lastEnemyAttackedId : -1,
            lastEnemyAttackedCoords : undefined,
            playerCoords : undefined,
            gameScore : 10
        })
        var gameTimerCounter = setInterval(() => {
            let timeTemp = this.state.gameTimer
            this.setState({
                gameTimer: timeTemp + 1
            })
        }, 1000)

        var difficultyManager = setInterval(() =>{
            let currentDifficulty = this.state.difficulty
            if (currentDifficulty < 10){
                this.setState({
                    difficulty: currentDifficulty + 1
                })
            }
        }, 10000)

        var enemySpawnManager = setInterval(() => {
            let currentEnemyArray = this.state.enemyArray
            let enemyToSpawnID = this.getRandomIntInclusive(0, 10000000)
            currentEnemyArray.push(enemyToSpawnID)
            this.setState({
                enemyArray : currentEnemyArray
            })

        }, (2.8 + 0.26 * this.state.difficulty) * 1000) //Linear equasion to calculate spawn rate

        var gameEndChecker = setInterval(() => {
            if (this.state.gameScore < 0) {
                clearInterval(gameTimerCounter)
                clearInterval(difficultyManager)
                clearInterval(enemySpawnManager)
                clearInterval(gameEndChecker)
                this.endGame()
            }
        }, 200)
    }

    //Function that recieves data from player and enemies whenever attack occurs
    recieveAndWriteData(EntityData) {
        console.log(EntityData)
        let playerCoords = undefined
        let enemyId = undefined
        let enemyCoords = undefined
        if (EntityData.myId !== "PLAYER") {
            enemyCoords = EntityData.myCoords
            enemyId = EntityData.myId
            this.setState({
                lastEnemyAttackedId : EntityData.myId,
                lastEnemyAttackedCoords : EntityData.myCoords
            })
        }
        if (EntityData.myId === "PLAYER") {
            this.setState({
                playerCoords : EntityData.myCoords
            })
            playerCoords = EntityData.myCoords
        }
        this.collisionCheck(playerCoords, enemyCoords, enemyId)
    }

    collisionCheck(playerCoords, enemyCoords, enemyId) {
        if (this.state.lastEnemyAttackedCoords && this.state.playerCoords){
            if (playerCoords === undefined) {
                playerCoords = this.state.playerCoords
            }
            if (enemyCoords === undefined) {
                enemyCoords = this.state.lastEnemyAttackedCoords
            }
            if (enemyId === undefined) {
                enemyId = this.state.lastEnemyAttackedId
            }
            //DETECTING COLLISION
            let xOffset = 25
            let yOffset = 45
            if (playerCoords.x > (enemyCoords.x - xOffset) &&
                playerCoords.x < (enemyCoords.x + xOffset) &&
                playerCoords.y > (enemyCoords.y - yOffset) &&
                playerCoords.y < (enemyCoords.y + yOffset)
            ) {
                let newEnemyArray = []
                this.state.enemyArray.forEach(element => {
                    if (element === enemyId) {
                        element = 'dead'
                    }
                    newEnemyArray.push(element)             
                });
                this.setState({
                    enemyArray : newEnemyArray
                })
                this.checkScore()
            }
        }
    }

    //To be optimized not to iterate whole array
    checkScore() {
        let score = 10
        this.state.enemyArray.forEach(element => {
            if (element === 'dead'){
                score += 1
            }
            else if (element === 'passed') {
                score -= 2
            }
        });
        this.setState({
            gameScore : score
        })
    }

    //Called from Enemy component whenever enemy reaches right boundary
    enemyPassed(id) {
        let newEnemyArray = []
        this.state.enemyArray.forEach(element => {
            if (element === id) {
                element = 'passed'
            }
            newEnemyArray.push(element)             
        })
        this.setState({
            enemyArray : newEnemyArray
        })
        this.checkScore()
    }

    startButtonPressed() {
        console.log("GAME STARTED")
        this.setState({
            isGameActive : true,
            isGameFinished : false
        })
        this.gameLoop()
    }

    endGame() {
        console.log('GAME FINISHED')
        this.setState({
            isGameFinished : true,
            isGameActive : false
        })
    }

    componentWillUnmount() {
        this.endGame()
    }

    render() {
        if (this.state.isGameActive){
            return (
                <div>
                    <ScoreBar score = {this.state.gameScore} />
                    <TimerBar timer = {this.state.gameTimer} />
                    <Player onClick = {this.recieveAndWriteData} isGameActive = {this.state.isGameActive}/>
                    {this.state.enemyArray.map((element) => {
                        if (element !== 'dead' && element !== 'passed'){
                            return (
                                <Enemy id = {element} onClick = {this.recieveAndWriteData} difficulty = {this.state.difficulty} passedReciever = {this.enemyPassed}/>
                            )
                        }
                    })}
                </div>
            )
        } else if (!this.state.isGameActive && !this.state.isGameFinished) {
            return(
                <div>
                    <Player onClick = {this.recieveAndWriteData} isGameAtive = {this.state.isGameActive}/>
                    <GameButton text = 'START' onClick = {this.startButtonPressed}/>
                </div>
            )
        } else if (this.state.isGameFinished) {
            return(
                <div>
                <Player onClick = {this.recieveAndWriteData} isGameAtive = {this.state.isGameActive}/>
                <GameButton text = 'RESTART' onClick = {this.startButtonPressed}/>
            </div>
            )
        }
        
    }
}

