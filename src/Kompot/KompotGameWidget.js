import React from 'react'
import Game from './Components/Game'
import { GameWindow } from './Styles/Styles.style'

export default function KompotGameWidget() {
    return (
        <GameWindow>
        <div id="gameWindow">
            <Game />
        </div>
        </GameWindow>
    )
}
