import styled from 'styled-components'

export const GameWindow = styled.div`
    background-color: grey;
    margin: auto;
    height: 600px;
    width: 800px;
    border: 3px solid black;
`

export const ScoreBarDivStyle = styled.div`
    position: absolute;
    margin: 0px;
    margin-left: 10px;
    padding: 0px;
    z-index: 1000;
`

export const ScoreDisplayBarStyle = styled.h2`
    margin-top: -25px;
    font-size: 20px;
`

export const TimerBarDivStyle = styled.div`
    position: absolute;
    margin-left: 640px;
    padding: 0px;
    z-index: 1000;
`

export const TimerDisplayBarStyle = styled.h2`
    margin-top: -25px;
    font-size: 20px;
`

export const GameButtonStyled = styled.button`
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    padding: 10px;
    font-size: 50px;
    font-weight: 700;
    background-color: aquamarine;
    border: 3px solid black;
    border-radius: 10px;
    :hover {
            background-color: #6DDBB6;
            color: #191919;
        }
    `
