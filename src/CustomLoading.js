import { React, useState } from 'react'

export default function CustomLoading() {
    const [loading, setLoading] = useState('Loading')

    function changeLoading(){
        if (loading !== 'Loading.....'){
            setLoading(loading + '.')
        } else {
            setLoading('Loading')
        }
        
    }


    setTimeout(changeLoading,
        100
    )


    return (
        <div>
            <h1>{loading}</h1>
        </div>
    )
}
