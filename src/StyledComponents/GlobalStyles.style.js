import { createGlobalStyle } from 'styled-components'

export const GlobalStyles = createGlobalStyle`
    body {
        background-color: lightcyan;
        margin: 0;
        padding: 0;
    }
`