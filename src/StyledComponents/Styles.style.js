import styled from 'styled-components'

export const HeaderStyled = styled.div`
    background-color: cyan;
    padding: 5px;
    font-weight: 800;

    & table {
        margin: auto;
        border: 5px solid grey;
        & td {
            padding-left: 10px;
            padding-right: 10px;
            padding-top: 3px;
            padding-bottom: 3px;
        }
    }
`

export const MenuButtonStyled = styled.a`
    & a {
        text-decoration: none;
        color: green;
        & :visited {
            color: inherit;
        }
        & :hover {
            background-color: aquamarine;
            box-shadow: 0px 0px 0px 2px black inset
        }
    }
`

export const ArticleStyled = styled.div`
    padding: 10px;
    & h1 {
        text-align: center;
    }
    & p {
        font-weight: 500;
        text-align: center;
    }
`

export const TableStyled = styled.div`
    margin: auto;
    max-width: 70vw;
    border: 1px solid black;
`
