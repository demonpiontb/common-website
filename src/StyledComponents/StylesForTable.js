export const customStyles = {
    rows: {
        style: {
            minHeight: '45px', // override the row height
        },
    },
    headCells: {
        style: {
            paddingLeft: '10px', // override the cell padding for head cells
            paddingRight: '10px',
            backgroundColor: '#EDE6D6',
            fontWeight: '700'
        },
    },
    cells: {
        style: {
            paddingLeft: '5px', // override the cell padding for data cells
            paddingRight: '5px',
        },
    },
};