import './converter.css'
import React from 'react'
import Select from 'react-select'
import { useState, useEffect } from 'react'
import { setStyles } from './customStyles'
import { StyledConverter } from './Converter.style'

export default function CurrencyConverterWidget() {
    const [exchangeRates, setExchangeRates] = useState({})
    const [codeToName, setCodeToName] = useState({})
    const [resultsList, setResultsList] = useState({})
    const [customStyles, setCustomStyles] = useState({})
    const [lastBase, setLastBase] = useState()
    const amountRef = React.createRef()
    const options = []    


    async function fillCodeToNameList() {
        const currencyCodeToNameResponse = await fetch("https://openexchangerates.org/api/currencies.json");
            const currencyCodeToNameData = await currencyCodeToNameResponse.json();
            let currencyCodeToNameList = {}
            Object.keys(currencyCodeToNameData).forEach(currencyCode =>{
                currencyCodeToNameList[currencyCode] = currencyCodeToNameData[currencyCode]
            })
        setCodeToName(currencyCodeToNameList)
    }


    async function fillExchangeRateList(){
        const exchangeRateResponse = await fetch("https://www.cbr-xml-daily.ru/latest.js");
        const exchangeRateData = await exchangeRateResponse.json();
        let currencyExchangeTempList = {}
        Object.keys(exchangeRateData.rates).forEach(currencyCode =>{
            currencyExchangeTempList[currencyCode.toString()] = exchangeRateData.rates[currencyCode]
        })
        currencyExchangeTempList["RUB"] = 1.0000
        setExchangeRates(currencyExchangeTempList)
    }


    function selectChangeHandler(selectedElement) {
        setLastBase(selectedElement)
        calculateResults(selectedElement)
    }

    //Crutch
    function menuOpenHandler() {
        setCustomStyles(setStyles('open'))
    }

    //Crutch 
    function menuCloseHandler() {
        setCustomStyles(setStyles('closed'))
    }


    function inputChangeHandler(e){
        let maxChars = 15   //Input chars limit value
        if (amountRef.current.value.length > maxChars) {
            amountRef.current.value = amountRef.current.value.slice(0, maxChars); 
        }
        calculateResults()
    }

    //Whenever selectChangeHandler or inputChangeHandler trggered - this function is executed
    function calculateResults(selectedValue = lastBase){
        let selectedCurrencyCode = selectedValue ? selectedValue.value : lastBase //Can't use refs with <Select /> as it's async and shows previous value
        let selectedCurrencyAmount = amountRef.current.value
        let tempResultList = {}
        if(selectedCurrencyCode !== undefined && selectedCurrencyAmount >= 0){
            Object.keys(exchangeRates).forEach(currencyCode => {
                if(currencyCode !== selectedCurrencyCode){
                    tempResultList[currencyCode] = ((selectedCurrencyAmount / exchangeRates[selectedCurrencyCode]) * exchangeRates[currencyCode]).toFixed(4)
                }
            })
        } else if (selectedCurrencyCode === undefined) {
            tempResultList['Please, select currency'] = ''
        } else if (selectedCurrencyAmount < 0) {
            tempResultList['Amount should be positive'] = ''
        }

        setResultsList(tempResultList)
    }

    //Filling options array
    Object.keys(exchangeRates).map(currencyCode => {
        options.push({ value: currencyCode, label : codeToName[currencyCode]}) }
    )

    //Functions to execute on component mounted
    useEffect(() => {
        fillCodeToNameList()
        fillExchangeRateList()
        menuCloseHandler()
    }, [])

    
    return (
        <StyledConverter>
        <div className = 'converterContainer'>
            <div className = 'selectorContainer'>
                <Select
                        defaultValue = {options[0]}
                        onChange = {selectChangeHandler}
                        onMenuOpen = {menuOpenHandler}
                        onMenuClose = {menuCloseHandler}
                        options = {options}
                        styles = {customStyles}
                        isSearchable = {false}
                        blurInputOnSelect = {true}
                        placeholder = {'Select currency'}
                    />
            </div>
            <div className = 'inputContainer'>
                <input className = 'inputAmount' type="number" max="100" placeholder = 'Input amount' ref = {amountRef} onChange = {inputChangeHandler}></input>
            </div>
            <div className = 'resultsContainer'>
                <table className = 'resultTable'>
                    <tbody className = 'resultTableBody'>
                        {Object.keys(resultsList).map(currencyCode => {
                                return(
                                    <tr className="resultsTR" key = {currencyCode}>
                                        <td className = "resultsLEFT">{currencyCode}</td>
                                        <td className = "resultsRIGHT">{resultsList[currencyCode]}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        </div>
        </StyledConverter>
    )
}
