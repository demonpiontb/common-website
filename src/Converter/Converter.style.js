import styled from 'styled-components'

export const StyledConverter = styled.div`
    .converterContainer {
    margin-left: 10px;
    margin-right: 10px;
    max-width: 330px;
    margin: auto;
    }

    .selectorContainer{
        margin-top: 10px;
        margin-bottom: 10px;
        font-size: 120%;
        text-align: left;
    }

    .inputContainer {
        margin-bottom: 10px;
    }

    .inputAmount {
        margin:auto;
        font-size: 120%;
        width: 314px;
        padding:5px;
        border: 3px solid rgb(192, 192, 192);
        border-radius: 4px;
    }

    .inputAmount:hover {
        margin:auto;
        font-size: 120%;
        width: 314px;
        padding:5px;
        border: 3px solid rgb(65, 65, 65);
        border-radius: 4px;
    }

    .inputAmount:focus {
        outline:none;
        margin:auto;
        font-size: 120%;
        width: 314px;
        padding:5px;
        border: 3px solid rgb(0, 185, 0);
        border-radius: 4px;
    }

    .resultsContainer {
        margin-bottom: 10px;
        max-height: 300px;
        width: 330px;
    }

    .resultTable {
        margin:auto;
        font-size: 120%;
        width: 330px;
        height: 300px;
        border: 0px
    }

    .resultTableBody{
        display: inline-block;
        width: 330px; 
        height: 330px; 
        overflow-y: auto;
        overflow-x: hidden;
    }

    .resultsTR {
        display: inline-block;
        width: 330px;
        border:none;
    }

    .resultsTR:hover {
        width: 330px;
        background-color: rgb(130, 255, 130);
        border:none;
    }

    .resultsLEFT {
        float: left;
        text-align: left;
    }

    .resultsRIGHT {
        float: right;
        padding-right: 6px;
        text-align: right;
    }
    /* Disable arrows on input type number */
    & input[type="number"] {
        -moz-appearance: textfield;
        -webkit-appearance: textfield;
        appearance: textfield;
    }
    
    & input[type="number"]::-webkit-outer-spin-button,
    & input[type="number"]::-webkit-inner-spin-button {
        display: none;
    }
`