export function setStyles(isOpen){
    var customStyles = {}
    if(isOpen === 'open'){
        customStyles = {
            dropdownIndicator: () => ({
                display: 'none'
            }),
            menu: () => ({
                color: 'white',
                backgroundColor: 'black',
                position: 'absolute',
                width: '330px'
            }),
            menuList: (provided) => ({
                ...provided,
                "::-webkit-scrollbar": {
                    width: "10px"
                  },
                  "::-webkit-scrollbar-track": {
                    background: "rgba(0, 0, 0, 0.2)"
                  },
                  "::-webkit-scrollbar-thumb": {
                    boxShadow: 'inset 0 0 10px 10px grey',
                    border: 'solid 3px transparent'
                  },
                  "::-webkit-scrollbar-thumb:hover": {
                    boxShadow: 'inset 0 0 10px 10px grey',
                    border: 'solid 3px transparent'
                  }
                  
            }),
            control: (provided, state) => ({
                ...provided,
                backgroundColor: 'black',
                border: "3px solid rgb(0, 0, 0)",
                borderRadius: 0,
                boxShadow: state.isFocused ? 0 : 0,
                '&:hover': {
                   border: "3px solid rgb(0, 0, 0)"
                }
            }),
            singleValue: (provided) => ({
                ...provided,
                color: 'white',
                margin: '-3px',
                marginTop: '-7px',
            }),
            indicatorSeparator: () => ({
                display: 'none'
            }),
            option: (provided, state) => ({
                ...provided,
                backgroundColor: state.isDisabled
                    ? undefined
                    : state.isSelected
                    ? "rgb(0, 253, 0)"
                    : state.isFocused
                    ? "rgba(0, 253, 0, 0.4)"
                    : undefined  
            }),
            placeholder: (provided) => ({
                ...provided,
                margin: '-3px',
                marginTop: '-7px',
                color: 'rgb(117,117,117)'
            })
          }
    }
    else {
        customStyles = {
            dropdownIndicator: () => ({
                display: 'none'
            }),
            menu: () => ({
                color: 'white',
                backgroundColor: 'black'
            }),
            control: (provided, state) => ({
                ...provided,
                backgroundColor: 'white',
                border: "3px solid rgb(192, 192, 192)",
                boxShadow: state.isFocused ? 0 : 0,
                '&:hover': {
                   border: "3px solid rgb(65, 65, 65)"
                }
            }),
            indicatorSeparator: () => ({
                display: 'none'
            }),
            singleValue: (provided) => ({
                ...provided,
                color: 'black',
                margin: '-3px',
                marginTop: '-7px',
            }),
            placeholder: (provided) => ({
                ...provided,
                margin: '-3px',
                marginTop: '-7px',
                color: 'rgb(117,117,117)'
            })
          }
    }
    return customStyles
    
}
