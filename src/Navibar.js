import React from 'react'
import { Link } from 'react-router-dom'
import { MenuButtonStyled } from './StyledComponents/Styles.style'

export default function Navibar() {
    return (
        <div>
            <table>
                <tr>
                    <MenuButtonStyled><Link to = '/' exact = {true}><td>Home</td></Link></MenuButtonStyled>
                    <MenuButtonStyled><Link to = '/converter'  exact = {true}><td>Converter</td></Link></MenuButtonStyled>
                    <MenuButtonStyled><Link to = '/kompot' exact = {true}><td>Kompot</td></Link></MenuButtonStyled>
                    <MenuButtonStyled><Link to = '/contacts'  exact = {true}><td>Contacts</td></Link></MenuButtonStyled>
                    <MenuButtonStyled><Link to = '/about'  exact = {true}><td>About</td></Link></MenuButtonStyled>
                </tr>
            </table>
        </div>
    )
}
