import React from 'react'
import CurrencyConverterWidget from '../Converter/CurrencyConverterWidget'

export default function CurrencyConverter() {
    return (
        <div>
            <CurrencyConverterWidget />
        </div>
    )
}
