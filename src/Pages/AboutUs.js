import React from 'react'
import { ArticleStyled } from '../StyledComponents/Styles.style'

export default function AboutUs() {
    return (
        <div>
            <ArticleStyled>
            <h1>About</h1>
            <p>
                This is a website made on React JS for testing purposes.
            </p>
            </ArticleStyled>
        </div>
    )
}
