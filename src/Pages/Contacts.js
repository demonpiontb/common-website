import { React, useState, useEffect } from 'react'
import { ArticleStyled, TableStyled } from '../StyledComponents/Styles.style'
import DataTable from 'react-data-table-component';
import { customStyles } from '../StyledComponents/StylesForTable'
import CustomLoading from '../CustomLoading';


export default function Contacts() {
    const [data, setData] = useState([])
    const [pending, setPending] = useState(true)

    const columns = [
        {
            name: 'Title',
            selector: row => row.title,
            sortable: false,
        },
        {
            name: 'Phone',
            selector: row => row.phone,
            sortable: false,
        },
        {
            name: 'Adress',
            selector: row => row.adress,
            sortable: false,
        },
        {
            name: 'Average reply time (h)',
            selector: row => row.reply,
            sortable: true,
        }
    ];

    function loadData(){
        let tempData = []
        tempData = [
            {
                id: 1,
                title: 'My Home',
                phone: '+7 904 047 92 22',
                adress: 'Russia, Dzerzhinsk',
                reply: '0,5'
            },
            {
                id: 2,
                title: 'My Work',
                phone: '+7 904 047 92 22',
                adress: 'Russia, Dzerzhinsk',
                reply: '1'
            },
            {
                id: 3,
                title: 'Nizhny branch',
                phone: '+7 904 047 92 22',
                adress: 'Russia, Nizhny Novgorod',
                reply: '0,2'
            },
            {
                id: 4,
                title: 'German branch',
                phone: '+49 171 1234 567',
                adress: 'Germany, Berlin',
                reply: '3'
            },
            {
                id: 5,
                title: 'US branch',
                phone: '+1 646-566-4742',
                adress: 'USA, New York',
                reply: '2'
            }
        ]
        setData(tempData)
        setPending(false)
    }
    useEffect(() => {
        setTimeout(loadData,
            2000
        )
    }, []);

    return (
        <div>
            <ArticleStyled>
            <h1>Contacts</h1>
            <p>
                In the table below you can see fake contacts.
            </p>
            </ArticleStyled>
            <TableStyled>
            <DataTable
                columns={columns}
                data={data}
                customStyles={customStyles}
                progressPending = {pending}
                progressComponent = {<CustomLoading />}
            />
            </TableStyled>
        </div>
    )
}
