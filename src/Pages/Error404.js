import React from 'react'

export default function Error404() {
    return (
        <div>
            <h1>ERROR 404</h1>
            <img src='https://i.playground.ru/p/c4dXB70AYo64KAwiHmkC5w.jpeg' alt = '404 ERROR'></img>
        </div>
    )
}
