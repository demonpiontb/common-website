import React from 'react'
import { ArticleStyled } from '../StyledComponents/Styles.style'

export default function Home() {
    return (
        <div>
            <ArticleStyled>
            <h1>Welcome!</h1>
            <p>
                Behold! My first react multi-page app. <br/>
                You're at the homepage now, feel free to visit my currency converter (my mom said it's the best one).
                Also read "About" and "Contact" at corresponding menu buttons.                
            </p>
            </ArticleStyled>
        </div>
    )
}
