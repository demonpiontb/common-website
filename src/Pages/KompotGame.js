import React from 'react'
import { ArticleStyled } from '../StyledComponents/Styles.style'
import KompotGameWidget from '../Kompot/KompotGameWidget'

export default function KompotGame() {
    return (
        <div>
            <ArticleStyled>
            <h1>Kompot Game</h1>
            <p>
                Use mouse to control Kompot. Use LMB to attack. Try to aim heads. Score must be positive.
            </p>
            <KompotGameWidget />
            </ArticleStyled>
        </div>
    )
}
